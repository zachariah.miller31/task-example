FROM cgr.dev/chainguard/opentf as opentf
FROM fedora as build

#RUN <<EOF 
ENV mntbase="/tmpbase"
# Make this heredoc for actual docker
RUN sudo mkdir -p "$mntbase" && \
sudo dnf --noplugins update -y  && \
sudo dnf --installroot="${mntbase}" --noplugins --releasever 39 --setopt install_weak_deps=false --nodocs -y \
install coreutils-single glibc-minimal-langpack go-task bash openssl curl tar unzip wget && \
sudo dnf --noplugins --installroot="${mntbase}" clean all && \
sudo rm -rf /mnt/rootfs/var/cache/* /mnt/rootfs/var/log/dnf* /mnt/rootfs/var/log/yum.* 
#EOF

FROM scratch
LABEL maintainer="Zachariah Miller"

LABEL coffee.buzz.component="fubi39-micro-container"
LABEL name="fubi39/fubi-micro"
LABEL version="39"
LABEL description="Very small image which doesn't install the package manager."
COPY --from=build /tmpbase/ /
COPY --from=build /etc/yum.repos.d/* /etc/yum.repos.d/*
COPY --from=opentf /usr/bin/opentf /usr/local/bin/
ADD provider_versions /provider_versions
ADD install_tf_providers.sh /
RUN chmod +x /install_tf_providers.sh && /install_tf_providers.sh
CMD /bin/bash