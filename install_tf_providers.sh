#!/bin/bash

if [[ $DEBUG ]]; then set -x;fi

set -eEo pipefail

install-qemu () {
    
    echo 'DPkg::Post-Invoke {"/bin/rm -f /var/cache/apt/archives/*.deb || true";};' | tee /etc/apt/apt.conf.d/clean
    apt-get update -qq 
    apt-get install -qq --no-install-recommends qemu-user-static qemu-utils ca-certificates dosfstools e2fsprogs gdisk kpartx parted libarchive-tools sudo xz-utils psmisc wget
    rm -rf /var/lib/apt/lists/*
    wget https://raw.githubusercontent.com/qemu/qemu/master/scripts/qemu-binfmt-conf.sh
    wget https://raw.githubusercontent.com/multiarch/qemu-user-static/master/containers/latest/register.sh
    mv register.sh register
    chmod +x qemu-binfmt-conf.sh
    chmod +x register
}

install-buildah () {

    . /etc/os-release
    sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x${ID^}_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
    wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/x${ID^}_${VERSION_ID}/Release.key -O Release.key
    apt-key add - < Release.key
    apt-get update -qq
    apt-get install buildah qemu-user-static -qq 
    sync
    sed -i 's/driver = \"overlay\"/driver = \"vfs\"/g' /etc/containers/storage.conf

}


install-terraform () {

    wget -q -nc "https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_${PLATFORM}_${ARCH}.zip"
    unzip -o terraform_${TF_VERSION}_${PLATFORM}_${ARCH}.zip -d "${BASE_PATH}usr/local/bin"
    rm terraform_${TF_VERSION}_${PLATFORM}_${ARCH}.zip

}

install-golang () {
    # golang
    wget -q -nc https://dl.google.com/go/go${GOLANG_VERSION}.linux-amd64.tar.gz 
    tar -C /usr/local -xzf go${GOLANG_VERSION}.linux-amd64.tar.gz
    rm go${GOLANG_VERSION}.linux-amd64.tar.gz
    PATH="${BASE_PATH}usr/local/go/bin:${PATH}"
    go version

}

install-tfsec () {

    wget -q -nc "https://github.com/aquasecurity/tfsec/releases/download/v${TFSEC_VERSION}/tfsec-linux-${ARCH}"
    mv "./tfsec-linux-$ARCH" "$BASE_PATH/usr/local/bin/tfsec"
    chmod +x "${BASE_PATH}usr/local/bin/tfsec"
}


install-terradoc () {

    wget https://terraform-docs.io/dl/${TERADOC_VERSION}/terraform-docs-${TERADOC_VERSION}-$(uname)-${ARCH}.tar.gz
    tar -xzf terraform-docs-${TERADOC_VERSION}-$(uname)-${ARCH}.tar.gz
    #tar -C /usr/local/bin
    chmod +x terraform-docs
    mv terraform-docs "${BASE_PATH}usr/local/bin"
    #chmod +x /usr/local/bin/terraform-docs
}

install-tf-providers () {
    if [[ "$BASE_PATH" == "/" ]]; then PROVIDER_FILE="${BASE_PATH}provider_versions"; else PROVIDER_FILE="./scripts/provider_versions"; fi
    readarray -t arr2 < <(cat "${PROVIDER_FILE}")
    for key in "${!arr2[@]}"; do
        if [[ "${arr2[$key]}" == *"/"* ]]; then 
            local PUBLISHER=$(echo "${arr2[$key]}" | awk -F "/" '{print $1=$1}' | tr '[:upper:]' '[:lower:]' | tr -d \")
            local PROVIDER_NAME=$(echo "${arr2[$key]}" | awk -F "=" '{print $1=$1}' | awk -F "/" '{print $1=$2}' | tr '[:upper:]' '[:lower:]' | tr -d \")
            echo $PUBLISHER
            echo $PROVIDER_NAME
        else
            local PUBLISHER="hashicorp"
            local PROVIDER_NAME=$(echo "${arr2[$key]}" | awk -F "=" '{print $1=$1}' | tr '[:upper:]' '[:lower:]' | tr -d \")
            echo $PUBLISHER
            echo $PROVIDER_NAME
        fi
        #local PROVIDER_NAME=$(echo "${arr2[$key]}" | awk -F "=" '{print $1=$1}' | tr '[:upper:]' '[:lower:]' | tr -d \")
        local PROVIDER_VERSION=$(echo "${arr2[$key]}" | awk -F "=" '{print $1=$2}' | tr '[:upper:]' '[:lower:]' | tr -d \")
        local PROVIDER_FILE="terraform-provider-${PROVIDER_NAME}_${PROVIDER_VERSION}_${PLATFORM}_${ARCH}"
        local PROVIDER_ZIP="${PROVIDER_FILE}.zip"
        local PROVIDER_PATH="${BASE_PATH}plugins/registry.terraform.io/${PUBLISHER}/${PROVIDER_NAME}/${PROVIDER_VERSION}/${PLATFORM}_${ARCH}/"
        local PROVIDER_URL="${HASHICORP_PROVIDER_URL}/terraform-provider-${PROVIDER_NAME}/${PROVIDER_VERSION}/${PROVIDER_ZIP}"
        #wget -q -nc ${PROVIDER_URL} .
        wget -q -nc ${PROVIDER_URL}
        mkdir -p "${PROVIDER_PATH}"
        unzip -o "${PROVIDER_ZIP}" -d "${PROVIDER_PATH}"
        rm $PROVIDER_ZIP
    done
}

echo $BASH_VERSION
CLOUD="$1"
TF_VERSION="$2"
HASHICORP_PROVIDER_URL="https://releases.hashicorp.com"
PLATFORM=linux
ARCH=amd64
BASE_PATH="/" # or $mntbase
PUBLISHER=hashicorp
TFSEC_VERSION="1.28.0"
TERADOC_VERSION="v0.16.0"
#install-buildah
#if [[ $CLOUD == "AWS" ]]; then install-aws-cli; fi
#if [[ $CLOUD == "GCP" ]]; then install-gcp-cli; fi
#if [[ $CLOUD == "AZURE" ]]; then install-azure-cli; fi
# install-terradoc
# install-tfsec
# install-terraform
install-tf-providers

#install-kubectl
#install-golang
#install-helm
#install-packer
#apt-get clean
